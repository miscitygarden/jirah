	<style>
	.personal-info{ position: relative; left:0px; top: 60px;
	background-color: rgba(55, 55, 55, 0.9); color: black; width: 750px; height: 450px}
	#personalul{ position: relative; left:0px;
	top: 60px; background-color:rgba(20, 20, 20, 0.9); color: black; width: 750px}
	.nav .navbar-inner { padding: 0;}
	.tab-content{ width: 750px; height: 350px; }
	.tab-pane{ width: 750px; color: white}

	</style>             
<div id="container" style="position: absolute; top: 260px; left: 0px">
             <ul class="nav  nav-pills" id="personalul">
                <li class="active"><a href="#first" data-toggle="tab">1.	JOB KNOWLEDGE</a></li>
                <li><a href="#second" data-toggle="tab">2.	QUALITY OF WORK</a></li>
                <li><a href="#third" data-toggle="tab">3.	QUANTITY OF WORK</a></li>
                <li><a href="#fourth" data-toggle="tab">4.	JUDGEMENT</a></li>
                <li><a href="#fifth" data-toggle="tab">5.	JOB ATTITUDE</a></li>
                <li><a href="#sixth" data-toggle="tab">6.	COOPERATION</a></li>
                <li><a href="#seventh" data-toggle="tab">7.	INITIATIVE</a></li>
                <li><a href="#eigth" data-toggle="tab">8.	INDUSTRY</a></li>
                <li><a href="#nine" data-toggle="tab">9.	MENTAL STATE</a></li>
                <li><a href="#ten" data-toggle="tab">10.	PERSONALITY</a></li>
                <li><a href="#eleven" data-toggle="tab">11.	ATTENDANCE AND PUNCTUALITY</a></li>
                <li><a href="#twelve" data-toggle="tab">12.	POTENTIAL</a></li>
                <li><a href="#thirteen" data-toggle="tab">13.	ADHERENCE TO COMPANY POLICIES</a></li>
                <li><a href="#fourteen" data-toggle="tab">14. Strenghts/Weakpoints/Remarks</a></li>
                <li><a href="#fifteen" data-toggle="tab">15. Remarks & Submit</a></li>
        
            </ul>
            
            <div class="tab-content personal-info">
    <div class="tab-pane active" id="first">
        <div class="container-fluid">
            <div class="profile2">
                               <input type="radio" name="app_jobknow" value="13" checked/>13	-	Exceptional<br/>
                <input type="radio" name="app_jobknow" value="11"/>11	-	Excellent<br/>
                <input type="radio" name="app_jobknow" value="9"/>09	-	Above Average<br/>
                <input type="radio" name="app_jobknow" value="7"/>07	-	Reasonably Adequate<br/>
                <input type="radio" name="app_jobknow" value="5"/>05	-	Insufficient on minor phase of the job<br/>
                <input type="radio" name="app_jobknow" value="3"/>03	-	Insufficient on major phase of the job<br/>
                <input type="radio" name="app_jobknow" value="1"/>01	-	Very little knowledge about the job.<br/>
            </div>
        </div>
    </div>
    
                
               <div class="tab-pane" id="second">
              
                <input type="radio" name="app_qualwork" value="13" checked/>13	-	Accuracy, neatness and thoroughness in work surpasses set
                standards  in an excellent exceptional manner.<br/>
                <input type="radio" name="app_qualwork" value="11"/>11	-	Superior quality of work is superior as compared both with  		other's work and that of the set standard of the company.<br/>
                <input type="radio" name="app_qualwork" value="9"/>09	-	Above average quality of work as compared to the set
                standards .<br/>
                <input type="radio" name="app_qualwork" value="7"/>07	-	Quality of   work is in conformity with the set and 
                acceptable  standard of the company.<br/>
                <input type="radio" name="app_qualwork" value="5"/>05	-	Quality of work is below acceptable standards.<br/>
                <input type="radio" name="app_qualwork" value="3"/>03	-	Quality of work is satisfactory and frequently commit the same error.<br/>
                <input type="radio" name="app_qualwork" value="1"/>01	-	Poor quality of work even with the full instruction and consistent reminders by the supervisor.<br/>
        
        
                </div>
                
                    <div class="tab-pane" id="third">
                <input type="radio" name="app_quanwork" value="13" checked/>13	-	Accomplishes exceptionally large amount of work ahead of time.<br/>
                <input type="radio" name="app_quanwork" value="11"/>11	-	Accomplishes more than average amount of work ahead of time.<br/>
                <input type="radio" name="app_quanwork" value="9"/>09	-	Accomplishes average amount of work   on  time.<br/>
                <input type="radio" name="app_quanwork" value="7"/>07	-	Submits  accomplished regular work on  time.<br/>
                <input type="radio" name="app_quanwork" value="5"/>05	-	Submits accomplished work after the targeted deadline.<br/>
                <input type="radio" name="app_quanwork" value="3"/>03	-	Submits accomplished work only if the supervisor will demand  after the deadline.<br/>
                <input type="radio" name="app_quanwork" value="1"/>01	-	Frequently submits accomplished work late which affects departmental operations.<br/>
                    
                </div>
                
                    <div class="tab-pane" id="fourth">
                <input type="radio" name="app_judge" value="12" checked/>12	-	Can be relied upon even beyond his job when others needs assistance  in decision making. 
                <br/>Can easily affect and influence  for the positive judgment and decision of co-employees<br/>
                <input type="radio" name="app_judge" value="10"/>10	-	Very accurate and prompt in judgment in terms of his own job.   Highly reliable.<br/>
                <input type="radio" name="app_judge" value="8"/>08	-	Quite accurate in judgment.  Can be effective in his work
                even with little supervision.<br/>
                <input type="radio" name="app_judge" value="6"/>06	-	Rates fairly in judgment<br/>
                <input type="radio" name="app_judge" value="4"/>04	-	Not quite reliable in judgment<br/>
                <input type="radio" name="app_judge" value="2"/>02	-	Judgment in based/affected by others' opinion.  Need
                close watching to facilitate his work.
                    
                </div>
                
                    <div class="tab-pane" id="fifth">
                <input type="radio" name="app_jobatt" value="14" checked/>14	-	Exceptionally enthusiastic in performing the best job
                possible.<br/>
                <input type="radio" name="app_jobatt" value="12"/>12	-	With positive signs of full enthusiasm and enjoyment
                towards job assignment.<br/>
                <input type="radio" name="app_jobatt" value="10"/>10	-	Adequate interest in job.<br/>
                <input type="radio" name="app_jobatt" value="6"/>06	-	Needs a frequent morale booster by his supervisor 
                regarding his enthusiasm towards job.<br/>
                <input type="radio" name="app_jobatt" value="4"/>04	-	Finds difficulty in self-motivation and in enjoying his
                job.<br/>
                <input type="radio" name="app_jobatt" value="2"/>02	-	Enthusiasm towards job is at below par.  Wasting of time
                during office hours observed.
                    
                </div>
                
                
                    <div class="tab-pane" id="sixth">
                <input type="radio" name="app_coop" value="4" checked/>04	-	Unusual and strong force for office morale.  Give whole-
                hearted cooperation and inspire others to follow.<br/>
                <input type="radio" name="app_coop" value="3"/>03	-	Somewhat congenial and willingly cooperates with others.<br/>
                <input type="radio" name="app_coop" value="2"/>02	-	Can fairly get along with his co-employees and superiors.<br/>
                <input type="radio" name="app_coop" value="1"/>01	-	Follow orders but sometimes find to be difficult to work 
                with.<br/>
                <input type="radio" name="app_coop" value=".5" />.5	-	Inclined to be quarrelsome and uncooperative.  Has 
                tendency to ride rough-shod over others, upsets group 
                morale.<br/>
                </div>
                    
        
                    <div class="tab-pane" id="seventh">
                <input type="radio" name="app_init" value="4" checked/>04	-	Very resourceful in setting for himself additional
                task, makes exceptionally worthwhile suggestions, 
                highly self-reliant and follows through jobs in his own 
                violation.<br/>
                <input type="radio" name="app_init" value="3"/>03	-	Alert to opportunities and sets actions to effectively
                improve his work.<br/>
                <input type="radio" name="app_init" value="2"/>02	-	Can perform his regular work with minimum supervision.<br/>
                <input type="radio" name="app_init" value="1"/>01	-	Frequently waits for instructions coming from superior.<br/>
                <input type="radio" name="app_init" value=".5"/>.5	-	Cannot work effectively without close supervision and
                detailed instructions from superiors.<br/>
                </div>
                
                   <div class="tab-pane" id="eigth">
                <input type="radio" name="app_ind" value="4" checked/>04	-	Rarely equaled when doing his work.  Very energetic and conscientious.<br/>
                <input type="radio" name="app_ind" value="3"/>03	-	Basically a good worker who possess considerably attention with his work.<br/>
                <input type="radio" name="app_ind" value="2"/>02	-	Average attention and energy with hi work.<br/>
                <input type="radio" name="app_ind" value="1"/>01	-	Displays little energy in the performance of his work.<br/>
                <input type="radio" name="app_ind" value=".5"/>.5	-	Lacks energy and motivation.<br/>
                </div>
        
                <div class="tab-pane" id="nine">
                <input type="radio" name="app_ment" value="4" checked/>04	-	Exceptional ability to grasp instructions especially
                when pressured.<br/>
                <input type="radio" name="app_ment"value="3" />03	-	Above average ability to grasp instructions and
                maintains cheerful disposition.<br/>
                <input type="radio" name="app_ment" value="2"/>02	-	Can usually interpret and understand instructions given.<br/>
                <input type="radio" name="app_ment" value="1"/>01	-	FSomewhat slow to understand and effect instructions
                given.  Easily distracted when busy.<br/>
                <input type="radio" name="app_ment" value=".5" />.5	-	Cannot work at all when pressured and consumed
                with deadlines.<br/>
                </div>
        
                <div class="tab-pane" id="ten">
                <input type="radio" name="app_personality" value="3" checked/>03	-	Creates lasting impression.  Manner highly personable,
                Very easy to talk to.<br/>
                <input type="radio" name="app_personality" value="2.5"/>2.5	-	Pleasant personality, well-liked, self confident, cheerful,
                and puts others at ease.<br/>
                <input type="radio" name="app_personality" value="2"/>02	-	General effect is fairly adequate.  Quite considerate and
                helpful.<br/>
                <input type="radio" name="app_personality" value="1"/>01	-	Can get along reasonable well with co-employees but
                general appearance is not remarkable.<br/>
                <input type="radio" name="app_personality" value=".5"/>.5	-	Needs  improvement in personality to avoid resistance.<br/>
                </div>
        
                <div class="tab-pane" id="eleven">
                <input type="radio" name="app_attend" value="8" checked/>08	-	Perfect attendance at work.  Observe break-time
                periods strictly and exceptionally punctual in observing
                working hours.<br/>
                <input type="radio" name="app_attend" value="6"/>06	-	Above average attendance at work.  Rarely absent
                or late for work.  Do not absent himself for trivial
                reasons but only if it is necessary.<br/>
                <input type="radio" name="app_attend" value="4"/>04	-	Average in attendance and punctuality.<br/>
                <input type="radio" name="app_attend" value="2"/>02	-	Attendance and punctuality is below expectation. Has
                occasional lates,  under times and  absences.  Absents
                himself even for trivial reasons.<br/>
                <input type="radio" name="app_attend" value=".5"/>.5	-	Frequently absent and late from work without any advise
                to superior.  Extra time is spent for break periods and
                frequently observed doing nothing during office hours.<br/>
                </div>
        
                 <div class="tab-pane" id="twelve">
                <input type="radio" name="app_poten" value="4" checked/>04	-	Exceptionally capable to handle higher position with
                greater responsibilities.  Can effectively apply new ideas
                for Hotels' interest.<br/>
                <input type="radio" name="app_poten" value="3"/>03	-	Highly effective in performing present job and can be
                relied upon to handle more difficult responsibilities from
                time to time.<br/>
                <input type="radio" name="app_poten" value="2"/>02	-	Can adequately perform present job.<br/>
                <input type="radio" name="app_poten" value="1"/>01	-	Needs training more to enhance capacity for higher 
                position.<br/>
                <input type="radio" name="app_poten" value=".5"/>.5	-	Not yet ready for advancement.<br/>
                </div>
        
        
                <div class="tab-pane" id="thirteen">
                <input type="radio" name="app_adhere" value="4" checked/>04	-	Strictly adheres to hotel policies and regulations. No
                verbal or written warning.<br/>
                <input type="radio" name="app_adhere" value="3"/>03	-	More than average expectation in following set rules
                and regulations.<br/>
                <input type="radio" name="app_adhere" value="2"/>02	-	Follows policies on average.<br/>
                <input type="radio" name="app_adhere" value="1"/>01	-	Adherence to  policies is coupled with minor
                warning sanctions.<br/>
                <input type="radio" name="app_adhere" value=".5"/>.5	-	Has accumulated more than one violation with stern
                warning, reprimand or suspension.<br/>
                </div>
                
                 <div class="tab-pane" id="fourteen">

                    <div class="form col-xs-8">
                    <label>Strength:</label>
                    <textarea name="app_strength" class="form-control"></textarea>
                    </div>
                    <div class="form col-xs-8">
                    <label>Weakness:</label>
                    <textarea name="app_weak" class="form-control"></textarea>
                    </div>
                    <div class="form col-xs-8">
                    <label>Developmental Needs:</label>
                    <textarea name="app_needs" class="form-control"></textarea>
                    </div>
                </div>

                <div class="tab-pane" id="fifteen">
                  <script>
				 $(function() {
        $('#status').change(function(){
            $('.for').hide();
            $('#' + $(this).val()).show();
        });
    });
				 </script>

			<div class="form col-xs-8" style="top: 30px">
				<select id="status" style="color:black" class="form-control">
                <option value="reg">Regular Employee</option>
				<option value="probi">Probationary Employee</option>
                </select>
            
                <div id="probi" style="display:none" class="for">
                <input type="radio" name="app_recommend" value="For regularization"/>For regularization<br>
                <input type="radio" name="app_recommend" value="To end service"/>To end service<br>
                </div>
                <div id="reg" style="width: 400px" class="for">
                <input type="radio" name="app_recommend" value="Maintain in the present position"/>Maintain in the present position<br>
                <input type="radio" name="app_recommend" value="For merit increase"/>For merit increase<br>
                <input type="radio" name="app_recommend" value="For promotion"/>For promotion<br>
                <input type="radio" name="app_recommend" value="For transfer"/>For transfer, position: Department<br>
                <input type="radio" name="app_recommend" value="For memo of commendation but w/o increase"/>For memo of commendation but w/o increase<br>
                <input type="radio" name="app_recommend" value="For remedial action due to poor performance"/>For remedial action due to poor performance
                </div>

                
                
                <div class="form col-xs-8">
                <label>Remarks:</label>
                <textarea name="app_remarks" class="form-control"></textarea>
                </div>
                
                                <input type="text" value="<?php echo $_SESSION['username']; ?>"  name="app_username" hidden/>
                                
                <div class="form col-xs-8">
				<label>Submit:</label>
				<input type="submit" class="form-control btn-success"  value="Submit">
                </div>
            
                    </div>


                

 </div>