/**
 * Site : http:www.smarttutorials.net
 * @author muni
 */
	      
 $(".delete").on('click', function() {
	$('.case:checkbox:checked').parents("tr").remove();
	$('.check_all').prop("checked", false); 
	check();
});
var i=$('table tr').length;

$(".addmore").on('click',function(){
	count=$('table tr').length;
	
    var data="<tr><td><input type='checkbox' class='case'/></td><td><span id='snum"+i+"'>"+count+".</span></td>";
    data +="<td><input class='form-control' type='text' id='firstname_"+i+"' name='firstname[]'/></td> <td><input class='form-control' type='text' id='lastname_"+i+"' name='lastname[]'/></td><td><input class='form-control' type='text' id='a_department_"+i+"' name='a_department[]'/></td><td><input class='form-control' type='text' id='username_"+i+"' name='username[]'/></td></tr>";
	$('table').append(data);
	row = i ;
	$('#firstname_'+i).autocomplete({
  	source: function( request, response ) {
  		$.ajax({
  			url : 'ajax.php',
  			dataType: "json",
  			method: 'post',
			data: {
			   name_startsWith: request.term,
			   type: 'country_table',
			   row_num : row
			},
			 success: function( data ) {
				 response( $.map( data, function( item ) {
				 	var code = item.split("|");
					return {
						label: code[0],
						value: code[0],
						data : item
					}
				}));
			}
  		});
  	},
  	autoFocus: true,	      	
  	minLength: 0,
  	select: function( event, ui ) {
		var names = ui.item.data.split("|");
		id_arr = $(this).attr('id');
  		id = id_arr.split("_");					
		$('#lastname_'+id[1]).val(names[1]);
		$('#a_department_'+id[1]).val(names[2]);
		$('#username_'+id[1]).val(names[3]);
	}		      	
  });
  
  $('#username_'+i).autocomplete({
  	source: function( request, response ) {
  		$.ajax({
  			url : 'ajax.php',
  			dataType: "json",
  			method: 'post',
			data: {
			   name_startsWith: request.term,
			   type: 'country_table',
			   row_num : row
			},
			 success: function( data ) {
				 response( $.map( data, function( item ) {
				 	var code = item.split("|");
					return {
						label: code[3],
						value: code[3],
						data : item
					}
				}));
			}
  		});
  	},
  	autoFocus: true,	      	
  	minLength: 0,
  	select: function( event, ui ) {
		var names = ui.item.data.split("|");
        id_arr = $(this).attr('id');
  		id = id_arr.split("_");			
		$('#lastname_'+id[1]).val(names[1]);
		$('#a_department_'+id[1]).val(names[2]);
		$('#firstname_'+id[1]).val(names[0]);
	}		      	
  });
  $('#a_department_'+i).autocomplete({
  	source: function( request, response ) {
  		$.ajax({
  			url : 'ajax.php',
  			dataType: "json",
  			method: 'post',
			data: {
			   name_startsWith: request.term,
			   type: 'country_table',
			   row_num : row
			},
			 success: function( data ) {
				 response( $.map( data, function( item ) {
				 	var code = item.split("|");
					return {
						label: code[2],
						value: code[2],
						data : item
					}
				}));
			}
  		});
  	},
  	autoFocus: true,	      	
  	minLength: 0,
  	select: function( event, ui ) {
		var names = ui.item.data.split("|");
		id_arr = $(this).attr('id');
  		id = id_arr.split("_");						
		$('#lastname_'+id[1]).val(names[1]);
		$('#username_'+id[1]).val(names[3]);
		$('#firstname_'+id[1]).val(names[0]);
	}		      	
  });
  $('#lastname_'+i).autocomplete({
  	source: function( request, response ) {
  		$.ajax({
  			url : 'ajax.php',
  			dataType: "json",
  			method: 'post',
			data: {
			   name_startsWith: request.term,
			   type: 'country_table',
			   row_num : row
			},
			 success: function( data ) {
				 response( $.map( data, function( item ) {
				 	var code = item.split("|");
					return {
						label: code[1],
						value: code[1],
						data : item
					}
				}));
			}
  		});
  	},
  	autoFocus: true,	      	
  	minLength: 0,
  	select: function( event, ui ) {
		var names = ui.item.data.split("|");
		id_arr = $(this).attr('id');

  		id = id_arr.split("_");						
		$('#username_'+id[1]).val(names[3]);
		$('#a_department_'+id[1]).val(names[2]);
		$('#firstname_'+id[1]).val(names[0]);
		$('#a_position_'+id[1]).val(names[4]);
		
	}		      	
  });
  
	i++;
});
				
function select_all() {
	$('input[class=case]:checkbox').each(function(){ 
		if($('input[class=check_all]:checkbox:checked').length == 0){ 
			$(this).prop("checked", false); 
		} else {
			$(this).prop("checked", true); 
		} 
	});
}

function check(){
	obj=$('table tr').find('span');
	$.each( obj, function( key, value ) {
		id=value.id;
		$('#'+id).html(key+1);
	});
}
										
$('#firstname_1').autocomplete({
	source: function( request, response ) {
		$.ajax({
			url : 'ajax.php',
			dataType: "json",
			method: 'post',
			data: {
			   name_startsWith: request.term,
			   type: 'country_table',
			   row_num : 1
			},
			 success: function( data ) {
				 response( $.map( data, function( item ) {
				 	var code = item.split("|");
					return {
						label: code[0],
						value: code[0],
						data : item
					}
				}));
			}
		});
	},
	autoFocus: true,	      	
	minLength: 0,
	select: function( event, ui ) {
		var names = ui.item.data.split("|");						
		$('#lastname_1').val(names[1]);
		$('#a_department_1').val(names[2]);
		$('#username_1').val(names[3]);
		$('#a_position_1').val(names[4]);
	}		      	
});
			      
$('#username_1').autocomplete({
  	source: function( request, response ) {
  		$.ajax({
  			url : 'ajax.php',
  			dataType: "json",
  			method: 'post',
			data: {
			   name_startsWith: request.term,
			   type: 'username',
			   row_num : 1
			},
			 success: function( data ) {
				 response( $.map( data, function( item ) {
				 	var code = item.split("|");
					return {
						label: code[3],
						value: code[3],
						data : item
					}
				}));
			}
  		});
  	},
  	autoFocus: true,	      	
  	minLength: 0,
  	select: function( event, ui ) {
		var names = ui.item.data.split("|");					
		$('#lastname_1').val(names[1]);
		$('#a_department_1').val(names[2]);
		$('#firstname_1').val(names[0]);
		$('#a_position_1').val(names[3]);
	},
	open: function() {
		$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
	},
	close: function() {
		$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
	}		      	
 });
  
 $('#lastname_1').autocomplete({
  	source: function( request, response ) {
  		$.ajax({
  			url : 'ajax.php',
  			dataType: "json",
  			method: 'post',
			data: {
			   name_startsWith: request.term,
			   type: 'lastname',
			   row_num : 1
			},
			 success: function( data ) {
				 response( $.map( data, function( item ) {
				 	var code = item.split("|");
					return {
						label: code[1],
						value: code[1],
						data : item
					}
				}));
			}
  		});
  	},
  	autoFocus: true,	      	
  	minLength: 0,
  	select: function( event, ui ) {
		var names = ui.item.data.split("|");					
		$('#username_1 ').val(names[3]);
		$('#a_department_1').val(names[2]);
		$('#firstname_1').val(names[0]);
	},
	open: function() {
		$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
	},
	close: function() {
		$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
	}		      	
});
  
$('#a_department_1').autocomplete({
  	source: function( request, response ) {
  		$.ajax({
  			url : 'ajax.php',
  			dataType: "json",
  			method: 'post',
			data: {
			   name_startsWith: request.term,
			   type: 'a_department',
			   row_num : 1
			},
			 success: function( data ) {
				 response( $.map( data, function( item ) {
				 	var code = item.split("|");
					return {
						label: code[2],
						value: code[2],
						data : item
					}
				}));
			}
  		});
  	},
  	autoFocus: true,	      	
  	minLength: 0,
  	select: function( event, ui ) {
		var names = ui.item.data.split("|");					
		$('#username_1 ').val(names[3]);
		$('#lastname_1 ').val(names[1]);
		$('#firstname_1').val(names[0]);
	},
	open: function() {
		$( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
	},
	close: function() {
		$( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
	}		      	
});