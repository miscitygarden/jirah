<?php
session_start();
include('../lock.php');
if(!$_SESSION['username'])
{
	header("location: ../../index.php");
}

?>
<?php include ('file.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Jirah&trade;</title>
<link rel="stylesheet" href="css/style.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
</head>  
<script>
$(document).ready(function(){
$("#button2").css('color','black');
    $("#button").click(function(){
	$("#button").css('color','black');
	$("#button2").css('color','white');
	$("#button3").css('color','white');
	$("#1").show(500);
	$("#2").hide(500);
	$("#3").hide(500);
  });
  $("#button2").click(function(){
	$("#button").css('color','white');
	$("#button2").css('color','black');
	$("#button3").css('color','white');
    $("#1").hide(500);
	$("#2").show(500);
	$("#3").hide(500);
  }); 
  $("#button3").click(function(){
	$("#button").css('color','white');
	$("#button2").css('color','white');
	$("#button3").css('color','black');
    $("#2").hide(500);
	$("#1").hide(500);
	$("#3").show(500);
  }); 


});
</script>

<body>
<img src="../../images/penthouse5.jpg" class="bg">
<?php echo "<div class='titleno'>$rh_roomno </div>"; ?>

				<div id="choices">
        		<div class="nav-one" id="button"><a class="profile">Profile</a></div>
                <div class="nav-two" id="button2"><a class="viewall">View All</a></div>
                <div class="nav-three" id="button3"><a class="file">File/Input</a></div>
 				</div>
    
    
    <div id="1"  style="display:none">
        <?php include ('profile.php'); ?>
    </div>
    
    <div id="2">
        <?php include ('viewall.php'); ?>
    </div>

	
	<script type="text/javascript" src="js/script.js"></script>
	<script type="text/javascript">
	var sorter = new TINY.table.sorter('sorter','table',{
		headclass:'head',
		ascclass:'asc',
		descclass:'desc',
		evenclass:'evenrow',
		oddclass:'oddrow',
		evenselclass:'evenselected',
		oddselclass:'oddselected',
		paginate:true,
		size:10,
		colddid:'columns',
		currentid:'currentpage',
		totalid:'totalpages',
		startingrecid:'startrecord',
		endingrecid:'endrecord',
		totalrecid:'totalrecords',
		hoverid:'selectedrow',
		pageddid:'pagedropdown',
		navid:'tablenav',
		sortcolumn:1,
		sortdir:1,
		sum:[8],
		avg:[6,7,8,9],
		columns:[{index:7, format:'%', decimals:1},{index:8, format:'$', decimals:0}],
		init:true
	});
  </script>
  </div>  
 	 </div>
     
      <div id="3"  style="display:none">
  
  	<div class="wrapper">		
			
        <form action="insert.php" method="post" id="myform">
           
            	<div class="what">
				
				<label>Priority:</label>
                    <select name="rh_priority">
                   	 <option value="1">Priority 1</option>
                      <option value="2">Priority 2</option>
                      <option value="3">Priority 3</option>
                      <option value="4">Priority 4</option>
                      <option value="5">Priority 5</option>
                    </select>
            	</div>
                
        		<div class="datefiled">
				<label>Date Filed:</label>
				<input type="text" class="text" value="<?php echo date("M d, Y - D"); ?>" disabled></div>
				
                <div class="particulars">
				<label>Particulars:</label>
				<textarea type="text" class="tparticulars" name="rh_particulars"></textarea></div>

				<div class="department">
				<label>Concerning Department:</label>
                    <select name="rh_department">
                    <option value="Engineering">Engineering</option>
                    <option value="Housekeeping">Housekeeping</option>
                   
                    </select>
            	</div>
                
                <div class="status">
				<label>Status:</label>
                    <select name="rh_status">
                    <option value="Pending">Pending</option>
                   	 <option value="On-going">On-going</option>
                      <option value="Completed">Completed</option>
                    </select>
            	</div>
                

                <input type="hidden" name="rh_roomno" value="<?php echo $rh_roomno;?>" hidden>
                <input type="hidden" name="rh_userinput" value="<?php echo $_SESSION['username'];?>" hidden>
                
                 
                <div class="submit">
				<label>Submit:</label>
				<input type="submit" class="ssubmit"  value="Submit"></div>
			
           </div></form>
            
       
  </div>
        
        
    
     
</body>
</html>