<?php
session_start();

// set timeout period in seconds
$inactive = 3600;

// check to see if $_SESSION['timeout'] is set
if(isset($_SESSION['timeout']) ) {
	$session_life = time() - $_SESSION['timeout'];
	if($session_life > $inactive)
        { 
		// go to login page when idle
		session_destroy(); header("Location: ../logout.php"); 
	}
}
$_SESSION['timeout'] = time();
?>