<?php
include('reportfile.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Jirah&trade;</title>
<link rel="icon" type="image/png" href="../menu/images/fav.png" />
<link rel="stylesheet" type="text/css" href="../content/css/navstyle.css" />
<link href="../menu/css/simple-sidebar.css" rel="stylesheet">
<script src="../menu/js/jquery-1.9.1.js"></script>
<script src="../content/jquery.table2excel.js"></script>
</head>
<script>
var myVariable = <?php echo(json_encode($received)); ?>;
var myVariable2 = <?php echo(json_encode($mysql)); ?>;
</script>
<script type="text/javascript" src="../nav.js"></script>

<body>
<img src="../images/penthouse5.jpg" class="bg">
	

     
    </div>
    
<div id="wrapper" class="toggled" >            

          <div id="page-content-wrapper">
            <div class="container-fluid">
             <div class="row"> 
 
    <link href="../content/bootstrap-table.css" rel="stylesheet">
	<script src="../content/bootstrap-table.js"></script>


<div style="background-color: rgba(248, 248, 248, .85);width: 400px;   height: 220px;">
    <div class="row">
        <div class="form-group col-sm-10" style="position:absolute; left: 20px; top: 30px; width: 300px">
        <label>Report Module:</label>
        <p class="text-primary"><?php if ( $reportmodule != 'Roomhistory' ) {echo $reportmodule; } else { echo 'Room History'; } ?></p>
</div>

    </div>
    <div class="row">
    <div class="form-group col-xs-4" style="position:absolute; left: 20px; top: 80px">
    <?php if( $reportmodule != 'Joborder' and $reportmodule != 'Roomhistory' ){
    if ( $dhstatus=='A%') echo '<label>DH/Approval Status:</label><br><p class="text-primary">Approved</p>'; 
	elseif ($dhstatus=='R%') echo '<label>DH/Approval Status:</label><br><p class="text-primary">Reject</p>'; 
	else { echo '<label>DH/Approval Status:</label><br><p class="text-primary">Pending</p>';}  }
    else { if ($dhstatus=='C%') echo '<label>Status:</label><br><p class="text-primary">Completed</p>';
	 elseif ($dhstatus=='O%') echo '<label>Status:</label><br><p class="text-primary">Ongoing</p>'; 
	 else { echo '<label>Status:</label><br><p class="text-primary">Pending</p>';} }
    ?>
    </div>
    </div>
    <div class="row">
    <div class="form-group col-xs-4" style="position:absolute; left: 200px; top: 80px">
    <?php 
    if( $reportmodule != 'Joborder' and $reportmodule != 'Roomhistory' ){
    if ( $cgmstatus=='A%') echo '<label>Last Approval Status:</label><br><p class="text-primary"> Approved'; 
	elseif ($cgmstatus=='R%') echo '<label>Last Approval Status:</label><br><p class="text-primary"> Reject'; 
	else { echo '<label>Last Approval Status:</label><br><p class="text-primary"> Pending';}  }
    else { echo "<label>Room No:</label><br><p class='text-primary'>'.$roomno.'"; };?>
    </div>
    </div>
    <div class="row">
    <div class="form-group col-xs-4" style="position:absolute; left: 20px; top: 130px">
    <label>From:</label><br><p class="text-primary"><?php if ( $reportmodule == 'Leave' ) {echo $lfrom;} 
	else {echo $from; } ?></p>
    </div>
        <div class="form-group col-xs-4" style="position:absolute; left: 200px; top: 130px">
    <label>To:</label><br><p class="text-primary"> <?php if ( $reportmodule == 'Leave' ) {echo $lto;} 
	else {echo $to; } ?></p>
    </div>
        <div class="form-group col-xs-4" style="position:absolute; left: 20px; top: 180px">
    <label>User:</label> <br><p class="text-primary"><?php echo $username; ?></p>
    </div>
    </div>
    </div>
    <div class="row">
     <div class="form-group col-xs-4" style="position:absolute; left: 200px; top: 190px">
            <script>
                $(function() {
                    $("button").click(function(){
                    $("#table2excel").table2excel({
                        exclude: ".noExl",
                        name: "Jirah Report"
                    }); 
                     });
                });
            </script>
     <button class="btn btn-success">Export to Excel</button>
     </div>
    </div>
</div>

        <?php if ( $reportmodule =='Time')
		{ include ('overtime_rep.php'); }
		if ( $reportmodule =='Leave')
		{ include ('leave_rep.php'); }
		if ( $reportmodule =='Supplies')
		{ include ('supplies_rep.php'); }
		if ( $reportmodule =='Purchase')
		{ include ('purchased_rep.php'); }
		if ( $reportmodule =='Joborder')
		{ include ('joborder_rep.php'); }
		if ( $reportmodule =='Roomhistory')
		{ include ('roomhistory_rep.php'); }
		
		
		?>

                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->
</div>
    <!-- /#wrapper -->
         <link href="../menu/lib/css/bootstrap.min.css" rel="stylesheet">
        <script src="../menu/lib/js/bootstrap.min.js"></script>

        
    <!-- Menu Toggle Script -->
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>

  
</body>
</html>


