
<div class="modal fade" id="ModalShowEdit-<?php echo $account_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Edit User <?php echo 'for the Username: '.$firstname.'';?></h4>
      </div>
      <div class="modal-body">
 <form role="form" id="edituser" method="post" action="auth.php?action=edituser&id=<?php echo $account_id ?>" enctype="multipart/form-data" >
  <div class="row"> 
      <div class="form-group col-xs-2">
        <label for="title">Ref No:</label>
        <input type="text" class="form-control" name="account_id" value="<?php echo $account_id; ?>" disabled/>
      </div> 
      <div class="form-group col-xs-3">
        <label for="title">Username:</label>
        <input type="text" class="form-control"  name="username" value="<?php echo $username; ?>" 
        <?php if ($user_superadmin != 'yes'){ echo "disabled";} ?>/>
      </div> 

    </div>
    <div class="row"> 
          <div class="form-group col-xs-3">
        <label for="title">Firstname:</label>
        <input type="text" class="form-control"  name="firstname" value="<?php echo $firstname; ?>"/>
      </div> 
      <div class="form-group col-xs-3">
        <label for="title">Lastname:</label>
        <input type="text" class="form-control"  name="lastname" value="<?php echo $lastname; ?>" />
   	  </div> 
      <div class="form-group col-xs-3">
        <label for="title">Department:</label>
        <select name="a_department" class="form-control">
                     <?php
				$query_app="SELECT DISTINCT a_department FROM account order by a_department asc";
				$result_app=mysql_query($query_app);
				$num_app= mysql_numrows ($result_app);
				$i5=0;
				while ($i5 < $num_app) {
				$f15=mysql_result($result_app,$i5,"a_department");
				?> 
				<option value="<?php echo $f15;?>"><?php echo $f15;?></option>
                <?php
				$i5++;
				}
				?>
                      </select>
      </div>   

            <div class="form-group col-xs-3">
        <label for="title">Position:</label>
        <input type="text" class="form-control"  name="a_position" value="<?php echo $a_position; ?>"/>
      </div> 
    </div>
    <div class="row"> 
   	  <div class="form-group col-xs-3">
        <label for="title">Credit:</label>
        <input type="text" class="form-control"  name="a_credit" value="<?php echo $a_credit; ?>" />
      </div>
      <div class="form-group col-xs-3">
        <label for="title">Password:</label>
        <input type="text" class="form-control"  name="password" 
        value="<?php if ($user_superadmin == 'yes'){ echo $password;} else {echo '******';} ?>"/>
      </div> 

    </div>
    
<input type="submit" class="btn btn-success" id="submit"/>
</form>
      </div>
      <div class="modal-footer" style=" background-image: url(../img/city_title2.png);background-repeat: no-repeat;">
	<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    </div>
  </div>
</div>