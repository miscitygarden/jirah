<div class="container">            
        
            <ul class="tabs" align="center">	
                <li><a href="#tab1">1.	Planning, Organizing And Controlling</A></Li>
                <li><a href="#tab2">2.	Problem Solving And   Decision-Making</A></Li>
                <li><a href="#tab3">3.	Leadership</A></Li>
                <li><a href="#tab4">4.	Job Knowledge</A></Li>
                <li><a href="#tab5">5.	Results Orientation</A></Li>
                <li><a href="#tab6">6.	Quality  Of Work</A></Li>
                <li><a href="#tab7">7.	Sense Of Responsibility/Job  Attitude</A></Li>
                <li><a href="#tab8">8.	Interpersonal Skills</A></Li>
                <li><a href="#tab9">9.	Customer Relations</A></Li>
                <li><a href="#tab10">10.	Personality</A></Li>
                <li><a href="#tab11">11.	Adherence To Company Policies</A></Li>
                <li><a href="#tab12"> 	Strengths/Accomplishments</A></Li>
                <li><a href="#tab13">	Areas For Improvement/Developmental Needs</A></Li>
                <li><a href="#tab14">	Recommendation/Remarks</A></Li>

                
        
            </ul>
            <div class="tab_container">
                     <div id="tab1" class="tab_content">
                  Ability to plan, organize and       exercise   control; to provide 
                   directions towards achieving company goals and objectives<br/><br/>
                <input type="radio" name="planning" value="25" checked/>25	-	Excellent<br/>
                <input type="radio" name="planning" value="20"/>20	-	Very Good<br/>
                <input type="radio" name="planning" value="15"/>15	-	Satisfactory<br/>
                <input type="radio" name="planning" value="10"/>10	-	Average<br/>
                <input type="radio" name="planning" value="5"/>05	-	Needs Improvement<br/>
                    
                </div>
                
                <div id="tab2" class="tab_content">
              Ability to act immediately on potential problems that may affect productivity and disrupt company operations; ability to make sound decision based on company guidelines and policies <BR /><br/>
                 <input type="radio" name="solving" value="25" checked/>25	-	Excellent<br/>
                <input type="radio" name="solving" value="20"/>20	-	Very Good<br/>
                <input type="radio" name="solving" value="15"/>15	-	Satisfactory<br/>
                <input type="radio" name="solving" value="10"/>10	-	Average<br/>
                <input type="radio" name="solving" value="5"/>5	-	Needs Improvement<br/>
        
        
                </div>
                
                <div id="tab3" class="tab_content">
                Ability to inspire staff to improve levels of  performance; facilitates 
 teamwork from staff; ability to provide  feedback and 
 coaching to  subordinates, and to share know-how with 
 them<BR /><br/>

                <input type="radio" name="leadership" value="25" checked/>25	-	Excellent<br/>
                <input type="radio" name="leadership" value="20"/>20	-	Very Good<br/>
                <input type="radio" name="leadership" value="15"/>15	-	Satisfactory<br/>
                <input type="radio" name="leadership" value="10"/>10	-	Average<br/>
                <input type="radio" name="leadership" value="5"/>5	-	Needs Improvement<br/>
                    
                </div>
                
                <div id="tab4" class="tab_content">
                Knowledge on the nature 
 and details of the job,
 level of competence derived
 from training and practice
<BR /><br/>
                 <input type="radio" name="knowledge" value="25" checked/>25	-	Excellent<br/>
                <input type="radio" name="knowledge" value="20"/>20	-	Very Good<br/>
                <input type="radio" name="knowledge" value="15"/>15	-	Satisfactory<br/>
                <input type="radio" name="knowledge" value="10"/>10	-	Average<br/>
                <input type="radio" name="knowledge" value="5"/>5	-	Needs Improvement<br/>
                    
                </div>
                
                 <div id="tab5" class="tab_content">
                Production or delivery of targets<BR /><br/>
                 <input type="radio" name="orientation" value="25" checked/>25	-	Excellent<br/>
                <input type="radio" name="orientation" value="20"/>20	-	Very Good<br/>
                <input type="radio" name="orientation" value="15"/>15	-	Satisfactory<br/>
                <input type="radio" name="orientation" value="10"/>10	-	Average<br/>
                <input type="radio" name="orientation" value="5"/>5	-	Needs Improvement<br/>
                    
                </div>
                
                
                <div id="tab6" class="tab_content">
                Ability to meet the desired  
 output, accurate, no error & 
 work is presented in an 
 acceptable standard
<BR /><br/>
                 <input type="radio" name="quality" value="25" checked/>25	-	Excellent<br/>
                <input type="radio" name="quality" value="20"/>20	-	Very Good<br/>
                <input type="radio" name="quality" value="15"/>15	-	Satisfactory<br/>
                <input type="radio" name="quality" value="10"/>10	-	Average<br/>
                <input type="radio" name="quality" value="5"/>5	-	Needs Improvement<br/>
                </div>
                    
        
                <div id="tab7" class="tab_content">
Takes responsibilities for   actions and with positive outlook in performing the best job possible for the concern of   the company
<BR /><br/>
                 <input type="radio" name="attitude" value="25" checked/>25	-	Excellent<br/>
                <input type="radio" name="attitude" value="20"/>20	-	Very Good<br/>
                <input type="radio" name="attitude" value="15"/>15	-	Satisfactory<br/>
                <input type="radio" name="attitude" value="10"/>10	-	Average<br/>
                <input type="radio" name="attitude" value="5"/>5	-	Needs Improvement<br/>
                </div>
                
                 <div id="tab8" class="tab_content">
                Cooperation & ability to relate 
well with Top Management
/other Managers/employees/   
suppliers, etc.
<BR /><br/>
                 <input type="radio" name="interpersonal" value="25" checked/>25	-	Excellent<br/>
                <input type="radio" name="interpersonal" value="20"/>20	-	Very Good<br/>
                <input type="radio" name="interpersonal" value="15"/>15	-	Satisfactory<br/>
                <input type="radio" name="interpersonal" value="10"/>10	-	Average<br/>
                <input type="radio" name="interpersonal" value="5"/>5	-	Needs Improvement<br/>
                </div>
        
                 <div id="tab9" class="tab_content">
                Rapport with guests; tact and 
courteousness  in dealing with 
guests
<BR /><br/>
                 <input type="radio" name="relations" value="25" checked/>25	-	Excellent<br/>
                <input type="radio" name="relations" value="20"/>20	-	Very Good<br/>
                <input type="radio" name="relations" value="15"/>15	-	Satisfactory<br/>
                <input type="radio" name="relations" value="10"/>10	-	Average<br/>
                <input type="radio" name="relations" value="5"/>5	-	Needs Improvement<br/>
                </div>
        
                 <div id="tab10" class="tab_content">
                Ability to create a pleasing            
impression, winning 
confidence and mannerism
<BR /><br/>
                 <input type="radio" name="personality" value="25" checked/>25	-	Excellent<br/>
                <input type="radio" name="personality" value="20"/>20	-	Very Good<br/>
                <input type="radio" name="personality" value="15"/>15	-	Satisfactory<br/>
                <input type="radio" name="personality" value="10"/>10	-	Average<br/>
                <input type="radio" name="personality" value="5"/>5	-	Needs Improvement<br/>
                </div>
        
                 <div id="tab11" class="tab_content">
               Adherence to the policies   
and regulations of the 
company 
<BR /><br/>
                 <input type="radio" name="adherence" value="25" checked/>25	-	Excellent<br/>
                <input type="radio" name="adherence" value="20"/>20	-	Very Good<br/>
                <input type="radio" name="adherence" value="15"/>15	-	Satisfactory<br/>
                <input type="radio" name="adherence" value="10"/>10	-	Average<br/>
                <input type="radio" name="adherence" value="5"/>5	-	Needs Improvement<br/>
                </div>
                
                 <div id="tab12" class="tab_content">
               Strength:<textarea name="app_strength" class="strength"></textarea>
                Accomplishments:<textarea name="app_accomplish" class="strength"></textarea>
                </div>
        
                 <div id="tab13" class="tab_content">
               Areas for Improvement:<textarea name="app_improve" class="strength"></textarea>
               Developmental Needs:<textarea name="app_developmental" class="strength"></textarea>
                </div>
       
               

                 <div id="tab14" class="tab_content">
                  <script>
				 $(function() {
        $('#status').change(function(){
            $('.for').hide();
            $('#' + $(this).val()).show();
        });
    });
				 </script>

				<select id="status">
                <option value="reg">Regular Employee</option>
				<option value="probi">Probationary Employee</option>

                </select>
                <div id="probi" style="display:none" class="for">
                <input type="radio" name="app_recommend" value="For regularization"/>For regularization<br>
                <input type="radio" name="app_recommend" value="To end service"/>To end service<br>
                </div>
                <div id="reg" style="width: 400px" class="for">
                <input type="radio" name="app_recommend" value="Maintain in the present position"/>Maintain in the present position<br>
                <input type="radio" name="app_recommend" value="For merit increase"/>For merit increase<br>
                <input type="radio" name="app_recommend" value="For promotion"/>For promotion<br>
                <input type="radio" name="app_recommend" value="For transfer"/>For transfer, position: Department<br>
                <input type="radio" name="app_recommend" value="For memo of commendation but w/o increase"/>For memo of commendation but w/o increase<br>
                <input type="radio" name="app_recommend" value="For remedial action due to poor performance"/>For remedial action due to poor performance
                </div>
                <textarea name="app_remarks" class="remarks"></textarea>
                    <input type="submit" class="submit" value="submit" />
            
        
                </div>
                <input type="text" value="<?php echo $_SESSION['username']; ?>"  name="app_username" hidden/>
                