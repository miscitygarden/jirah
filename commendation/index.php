<?php
session_start();
include('lock.php');
if(!$_SESSION['username'])
{
	header("location: ../../main/");
}

include('../conn.php');
include('file.php');
?>


<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Jirah&trade;</title>
<link rel="stylesheet" type="text/css" href="../css/default.css" />
<link rel="icon" type="image/png" href="../../../../menu/images/fav.png" />
<link rel="stylesheet" type="text/css" href="../style.css" />
<link rel="stylesheet" type="text/css" href="../css/viewall/style.css" />
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<script>
$(document).ready(function(){
  $("#button1").click(function(){
	$("#2").show(500);
	$("#1").hide(500);
  });
  $("#button2").click(function(){
    $("#2").hide(500);
	$("#1").show(500);
  });


});
</script>

<body>
<img src="../../../images/penthouse5.jpg" class="bg">
<div class="nav">
    	<ul>
        	<li class="logo"><a href="../../../menu/"><img src="../../../menu/images/icon.png" border="0" /></a></li>
        	<li><a href="#">Welcome, <?php echo $a_firstname?>!!</a></li>
        	<li><a href="../">Appraisal</a></li>
             <?php if ($user_lastapproval == 'yes') echo '<li><a href="../dh.php">DH Appraisal</a></li>'; ?>
        	<li><a href="#"  style=" color: #00F; font-weight:bolder">Commendation</a></li>
        	<li><a href="#">Training</a></li>
        	<li><a href="#">Personal Action Form</a></li>
            <li><a href="../results">Results</a></li>
        	<li><a href="../../main/logout.php">Signout</a></li>
        </ul>
	 </div>
         <div id="choices">
        			
        		<div class="nav-two" id="button2"><a class="viewall">View All</a></div>
                <div class="nav-one" id="button1"><a class="file">File/Input</a></div>
           
    </div>
 
 
 
<div id="1" class="tab1">
		<div id="tableheader">
        	<div class="search2">
                   
                <select id="columns" onChange="sorter.search('query')"></select>
                <input type="text" id="query" onKeyUp="sorter.search('query')" />
                  
             
            </div>

            <span class="details">
				<div>Records <span id="startrecord"></span>-<span id="endrecord"></span> of <span id="totalrecords"></span></div>
        		<div><a href="javascript:sorter.reset()">reset</a></div>
        	</span>
        </div>
		<table cellpadding="0" cellspacing="0" border="0" id="table" class="tinytable">
            <thead>
            
                <tr>
                    <th><h3>Ref No</h3></th>
                    <th><h3>Firstname</h3></th>
                    <th><h3>Surname</h3></th>
                    <th><h3>Department</h3></th>
                    <th><h3>Subject</h3></th>
                    <th><h3>From</h3></th> 
                    <th><h3>Print</h3></th>
       
                </tr>
            </thead>
            <tbody>
            <?php

$i=0;
while ($i < $num) {

$f1=mysql_result($result,$i,"comm_id");
$f3=mysql_result($result,$i,"comm_firstname");
$f4=mysql_result($result,$i,"comm_lastname");
$f5=mysql_result($result,$i,"comm_department");
$f7=mysql_result($result,$i,"comm_from");
$f8=mysql_result($result,$i,"comm_date");
$f9=mysql_result($result,$i,"comm_subject");
$f10=mysql_result($result,$i,"comm_message");
$f11=mysql_result($result,$i,"comm_hr");
$f12=mysql_result($result,$i,"comm_hrposition");
$f13=mysql_result($result,$i,"comm_approved");
$f14=mysql_result($result,$i,"comm_aposition");
$f15=mysql_result($result,$i,"comm_user");

?>
                <tr>
                    <td><?php echo $f1; ?></td>
					<td><?php echo $f3; ?></td>
					<td><?php echo $f4; ?></td>
                    <td><?php echo $f5; ?></td>
					<td><?php echo $f9; ?></td>
					<td><?php echo $f7; ?></td>
					<td> <a onClick="window.open('print.php?p_id=<?php echo $f1; ?>',
                      'newwindow', 'width=800, height=500'); return false;" style=" color: blue; cursor: pointer">View</a></td>
                    
                    
                </tr>
                <?php
				$i++;
				}
				?>
            </tbody>
            
        </table>
        <div id="tablefooter">
          <div id="tablenav">
            	<div>
                    <img src="../css/viewall/images/first.gif" width="16" height="16" alt="First Page" onClick="sorter.move(-1,true)" />
                    <img src="../css/viewall/images/previous.gif" width="16" height="16" alt="First Page" onClick="sorter.move(-1)" />
                    <img src="../css/viewall/images/next.gif" width="16" height="16" alt="First Page" onClick="sorter.move(1)" />
                    <img src="../css/viewall/images/last.gif" width="16" height="16" alt="Last Page" onClick="sorter.move(1,true)" />
                </div>
                <div>
                	<select id="pagedropdown"></select>
				</div>
                <div>
                	<a href="javascript:sorter.showall()">view all</a>
                </div>
            </div>
			<div id="tablelocation">
            	<div>
                    <select onChange="sorter.size(this.value)">
                    <option value="5">5</option>
                        <option value="10" selected="selected">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div class="page">Page <span id="currentpage"></span> of <span id="totalpages"></span></div>
            </div>
        </div>
    </div>

	
	<script type="text/javascript" src="../css/viewall/script.js"></script>
	<script type="text/javascript">
	var sorter = new TINY.table.sorter('sorter','table',{
		headclass:'head',
		ascclass:'asc',
		descclass:'desc',
		evenclass:'evenrow',
		oddclass:'oddrow',
		evenselclass:'evenselected',
		oddselclass:'oddselected',
		paginate:true,
		size:10,
		colddid:'columns',
		currentid:'currentpage',
		totalid:'totalpages',
		startingrecid:'startrecord',
		endingrecid:'endrecord',
		totalrecid:'totalrecords',
		hoverid:'selectedrow',
		pageddid:'pagedropdown',
		navid:'tablenav',
		sortcolumn:1,
		sortdir:1,
		sum:[8],
		avg:[6,7,8,9],
		columns:[{index:7, format:'%', decimals:1},{index:8, format:'$', decimals:0}],
		init:true
	});
  </script>
                
</div>
       
<div id="2" style=" display:none">
       	 <div class="comm">   
            <h2>MEMO</h2>
             <form action="insert.php" method="post" id="myform">
             
            <div class="subject">
            Firstname		:	
            <input type="text" name="comm_firstname" placeholder="firstname" 
             size="25" id="country" onKeyUp="suggest(this.value);" onBlur="fill();fillId();" class="" > 
             <input type="hidden" name="country_id" id="country_id" value="" />
             <div class="suggestionsBox" id="suggestions" style="display: none;"> <div class="suggestionList" id="suggestionsList"> &nbsp; </div>
  </div>
	<br>
  			Lastname:<input type="text" name="comm_lastname"><br>

<div class="department">
				<label class="department2">Department:</label>
				<select name="comm_department" class="department3">
                    <option value="Accounting">Accounting</option>
                     <option value="Admin">Admin</option>
                     <option value="Frontoffice">Frontoffice</option>
                     <option value="Housekeeping">Housekeeping</option>
                     <option value="FnB">FnB</option>
                     <option value="Sales">Sales</option>
                     <option value="Engineering">Engineering</option>
                     <option value="Reservations">Reservations</option>
                     <option value="TC">Training Center</option>
                     <option value="Kitchen">Kitchen</option>
                     <option value="Security">Security</option>
                     <option value="HR">HR</option>
                     <option value="Graphics">Graphics</option>
                     <option value="MIS">MIS</option>
                      </select>
                      </div>
            FROM:<input type="text" name="comm_from" ><br>        
            <br><br>
            SUBJECT:<textarea type="text" name="comm_subject" class="subject2"></textarea> <br>
            </div>
            
            ____________________________________________________________________________ 
            <div class="body1">
            <textarea class="message" placeholder="Message" name="comm_message"></textarea><br><br>
            
            
            <input type="text" name="comm_hr" ><br>
            <input type="text" name="comm_hrposition" ><br><br><br>
            
            
            Noted By:<br>
            <input type="text" name="comm_approved"  ><br>
            <input type="text" name="comm_aposition" ><br>				
                                  
                          <input type="text" value="<?php echo $_SESSION['username']; ?>"  name="comm_user" hidden/>
                                  
           <input type="submit" value="submit">                    
           </form>
           </div>

</div>
</div>
		
        
          
      
</body>
</html>