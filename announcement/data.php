<div class="panel panel-primary" style=" position:absolute; left: 50px; width: auto; top: 100px">
  <div class="panel-body">
		
<div class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Add Adminstrator</h4>
      </div>
      <div class="modal-body">
	  
 <form role="form" id="addproduct" method="post" action="auth.php?action=insert" enctype="multipart/form-data" >
  <div class="form-group">
    <label for="title">Title:</label>
    <input type="text" class="form-control" id="title" name="title" placeholder="Title" required/>
  </div>
 
   <div class="form-group">
	   <label for="descr">Description</label>
       <textarea class="form-control" rows="5" id="descr" name="descr"></textarea>
   </div>
   
    
   <div class="form-group">
			<label for="desc">Attachment</label>
			<input type="hidden" name="MAX_FILE_SIZE" value="5242880" />
			<input name="uploadedfile" type="file" />
   </div>
   <div class="form-group">
			<label for="desc">Range of Date to Display</label>
	<input name="fromdate" id="fromdate" type="date" class="form-control" style=" display:  inline-block;width:170px"/> 
    <input name="todate" id="todate" type="date" class="form-control"  style="display:  inline-block;width:170px"/>
   </div>
    <input type="text"  name="p_username" id="p_username" value="<?php  echo $a_username; ?>" hidden/>
 <input type="submit" class="btn btn-primary" id="submit"/>
</form>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
    </div>
  </div>
</div>

<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">Add Announcement</button>

   <table data-toggle="table" data-sort-name="stargazers_count" data-sort-order="asc" data-pagination="true" data-show-columns="true" data-search="true" class="table table-bordered" style="background-color: rgba(255,255,255,.9)" data-show-export="true">
            <thead class="btn-primary" >
            		<th>ID</th>
            		<th>User</th>
					<th>Title</th>
					<th>Description</th>
					<th>Images</th>
					<th>From Date</th>
                    <th>To Date</th>
                    <th>Delete</th>
					
				</tr>
			</thead>
			<tbody>
			   <?php
				$mysqli = new mysqli($host, $user, $pass, $db);
				$str= "";
				if(isset($_POST['search']))
				{
					$sear=$_POST['search'];
					$str="select * from announcement";
				}
				else
				{
					$str="select * from announcement order by id desc";
				}
				if ($result = $mysqli->query($str))
					{
						while ($row = $result->fetch_object())
						{
							echo '<tr>';	
							echo '<td>'.$row->id.'</td>';	
							echo '<td>'.$row->user.'</td>';
							echo '<td style="width:150px;">'.$row->title.'</td>';
							echo '<td>'.$row->description.'</td>';
							echo '<td><img src="images/'.$row->images.'" width="100" height="100"></td>';

							echo '<td>'.$row->fromdate.'</td>';
							echo '<td>'.$row->todate.'</td>';
							echo '<td><a href="auth.php?action=delannounce&id='.$row->id.'"   onclick="setProdNo('.$row->id.');return confirmdelpro();">Delete</a></td>';						
							echo '</tr>';
					
						}
					}
			   ?>
			</tbody>
		</table>
	</div>
	</div>