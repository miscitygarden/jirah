<?php
session_start();
include('lock.php');
if(!$_SESSION['username'])
{
	header("location: ../../index.php");
}

?>
<?php include ('filedh.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Jirah&trade;</title>
<link rel="stylesheet" type="text/css" href="../css/default.css" />
<link rel="stylesheet" href="../css/viewall/style.css" />
</head>  

<body>
<img src="../../../images/penthouse5.jpg" class="bg">
<div class="nav">
    	<ul>
        	<li class="logo"><a href="../../../menu/"><img src="../../../menu/images/icon.png" border="0" /></a></li>
        	<li><a href="#">Welcome, <?php echo $a_firstname?>!!</a></li>
        	<li><a href="../">Appraisal</a></li>
             <?php if ($user_lastapproval == 'yes') echo '<li><a href="../dh.php">DH Appraisal</a></li>'; ?>
        	<li><a href="../commendation">Commendation</a></li>
        	<li><a href="#">Training</a></li>
        	<li><a href="#">Personal Action Form</a></li>
            <li><a href="index.php">Results</a></li>            
            <?php if ($user_lastapproval == 'yes') echo '<li><a href="#" style=" color: #00F; font-weight:bolder">DH Results</a></li>'; ?>
        	<li><a href="../../../main/logout.php">Signout</a></li>
        </ul>
	 </div>

     
    <div class="title">PERFORMANCE APPRAISAL RATING SCHEME</div>
    <div id="9">
    <div id="tablewrapper">
    
		<div id="tableheader">
        	<div class="search2">
                   
                <select id="columns" onchange="sorter.search('query')"></select>
                <input type="text" id="query" onkeyup="sorter.search('query')" />
                  
             
            </div>

            <span class="details">
				<div>Records <span id="startrecord"></span>-<span id="endrecord"></span> of <span id="totalrecords"></span></div>
        		<div><a href="javascript:sorter.reset()">reset</a></div>
        	</span>
        </div>
        <table cellpadding="0" cellspacing="0" border="0" id="table" class="tinytable">
            <thead>
            
                <tr>
                    <th><h3>Ref No</h3></th>
                    <th><h3>Firstname</h3></th>
                    <th><h3>Middlename</h3></th>
                    <th><h3>Surname</h3></th>
                    <th><h3>Department</h3></th>
                    <th><h3>Position</h3></th>
                    <th><h3>From</h3></th>
                    <th><h3>To</h3></th>
                    <th><h3>Total</h3></th>
                    <th><h3>By:</h3></th>
                    <th><h3>Classification:</h3></th>
                    <th><h3>Print</h3></th>
       
                </tr>
            </thead>
            <tbody>
            <?php

$i=0;
while ($i < $num) {

$f1=mysql_result($result,$i,"app_id");
$f2=mysql_result($result,$i,"app_firstname");
$f3=mysql_result($result,$i,"app_middlename");
$f4=mysql_result($result,$i,"app_lastname");
$f5=mysql_result($result,$i,"app_dept");
$f6=mysql_result($result,$i,"app_position");
$f7=mysql_result($result,$i,"app_from");
$f8=mysql_result($result,$i,"app_to");


$planning=mysql_result($result,$i,"planning");
$solving=mysql_result($result,$i,"solving");
$leadership=mysql_result($result,$i,"leadership");
$knowledge=mysql_result($result,$i,"knowledge");
$orientation=mysql_result($result,$i,"orientation");
$quality=mysql_result($result,$i,"quality");
$attitude=mysql_result($result,$i,"attitude");
$interpersonal=mysql_result($result,$i,"interpersonal");
$relations=mysql_result($result,$i,"relations");
$personality=mysql_result($result,$i,"personality");
$adherence=mysql_result($result,$i,"adherence");

$app_strength=mysql_result($result,$i,"app_strength");
$adherence=mysql_result($result,$i,"adherence");
$app_improve=mysql_result($result,$i,"app_improve");
$app_developmental=mysql_result($result,$i,"app_developmental");
$app_recommend=mysql_result($result,$i,"app_recommend");
$app_remarks=mysql_result($result,$i,"app_remarks");

$f10=mysql_result($result,$i,"app_username");

?>
<?php
$firstA= $planning + $solving + $leadership + $knowledge + $orientation + $quality + $attitude;
$first = $firstA*0.40 ;
$secondA=$interpersonal + $relations + $personality;
$second= $secondA * 0.32 ;
$third =$adherence*.24;


$total= $first + $second + $third;

?>
                <tr>
                    <td><?php echo $f1; ?></td>
					<td><?php echo $f2; ?></td>
					<td><?php echo $f3; ?></td>
					<td><?php echo $f4; ?></td>
                    <td><?php echo $f5; ?></td>
                    <td><?php echo $f6; ?></td>
					<td><?php echo $f7; ?></td>
					<td><?php echo $f8; ?></td>
                    <td><?php echo $total;?></td>
                    <td><?php echo $f10; ?></td>
                    <td><?php
					if ($total <= 100 && $total >= 89) {echo 'Excellent';}
					if ($total <= 88.9 && $total >= 76) {echo 'Very Good';}
					if ($total <= 75.9 && $total >= 63) {echo 'Satisfactory';}
					if ($total <= 62.9 && $total >= 50) {echo 'Average';}
					if ($total <= 49.9 && $total >= 37) {echo 'Needs Improvement';}
					if ($total <= 36.9 && $total >= 24) {echo 'Very Poor';}
					if ($total <= 23.9 && $total >= 11) {echo 'Very Poor';}
					?></td>
					<td> <a onClick="window.open('printdh.php?p_id=<?php echo $f1; ?>',
                      'newwindow', 'width=800, height=500'); return false;" style=" color: blue; cursor: pointer">View</a></td>
                    
                    
                </tr>
                <?php
				$i++;
				}
				?>
            </tbody>
            
        </table>
        <div id="tablefooter">
          <div id="tablenav">
            	<div>
                    <img src="../css/viewall/images/first.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                    <img src="../css/viewall/images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="../css/viewall/images/next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="../css/viewall/images/last.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
                </div>
                <div>
                	<select id="pagedropdown"></select>
				</div>
                <div>
                	<a href="javascript:sorter.showall()">view all</a>
                </div>
            </div>
			<div id="tablelocation">
            	<div>
                    <select onchange="sorter.size(this.value)">
                    <option value="5">5</option>
                        <option value="10" selected="selected">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div class="page">Page <span id="currentpage"></span> of <span id="totalpages"></span></div>
            </div>
        </div>
    </div>

	
	<script type="text/javascript" src="../css/viewall/script.js"></script>
	<script type="text/javascript">
	var sorter = new TINY.table.sorter('sorter','table',{
		headclass:'head',
		ascclass:'asc',
		descclass:'desc',
		evenclass:'evenrow',
		oddclass:'oddrow',
		evenselclass:'evenselected',
		oddselclass:'oddselected',
		paginate:true,
		size:10,
		colddid:'columns',
		currentid:'currentpage',
		totalid:'totalpages',
		startingrecid:'startrecord',
		endingrecid:'endrecord',
		totalrecid:'totalrecords',
		hoverid:'selectedrow',
		pageddid:'pagedropdown',
		navid:'tablenav',
		sortcolumn:1,
		sortdir:1,
		sum:[8],
		avg:[6,7,8,9],
		columns:[{index:7, format:'%', decimals:1},{index:8, format:'$', decimals:0}],
		init:true
	});
  </script>
  </div>
     
     
     
</body>
</html>