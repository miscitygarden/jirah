
<table data-toggle="table" data-sort-name="stargazers_count" data-sort-order="asc" data-pagination="true" data-show-columns="true"  data-page-list="[5, 10, 20, 50, 100, 200, 500, 1000]"
       data-search="true" class="table table-hover table-condensed table-bordered" style="background-color: rgba(255,255,255,.9);" data-show-export="true">
            <thead class="btn-primary" >
                <tr>
                    <th data-sortable="true" data-formatter="nameFormatter">Ref No</th>
                    <th data-sortable="true" data-formatter="nameFormatter">Firstname</th>
                    <th data-sortable="true" data-formatter="nameFormatter">Middlename</th>
                    <th data-sortable="true" data-formatter="nameFormatter">Surname</th>
                    <th data-sortable="true" data-formatter="nameFormatter">Department</th>
                    <th data-sortable="true" data-formatter="nameFormatter">Position</th>
                    <th data-sortable="true" data-formatter="nameFormatter">From</th>
                    <th data-sortable="true" data-formatter="nameFormatter">To</th>
                    <th data-sortable="true" data-formatter="nameFormatter">Total</th>
                    <th data-sortable="true" data-formatter="nameFormatter">By:</th>
                    <th data-sortable="true" data-formatter="nameFormatter">Classification:</th>
                    <th data-sortable="true" data-formatter="nameFormatter">Print</th>
       
                </tr>
            </thead>
            <tbody>
            <?php

$i=0;
while ($i < $num) {

$f1=mysql_result($result,$i,"app_id");
$f2=mysql_result($result,$i,"app_firstname");
$f3=mysql_result($result,$i,"app_middlename");
$f4=mysql_result($result,$i,"app_lastname");
$f5=mysql_result($result,$i,"app_dept");
$f6=mysql_result($result,$i,"app_position");
$f7=mysql_result($result,$i,"app_from");
$f8=mysql_result($result,$i,"app_to");

$f10=mysql_result($result,$i,"app_username");

?>
<?php
$total="SELECT SUM(app_jobknow + app_qualwork + app_quanwork + app_judge + app_jobatt + app_coop + app_init + app_ind + app_ment + app_personality + app_attend + app_poten + app_adhere) AS app_totals FROM appraisal where app_id='$f1'";
$total2=mysql_query($total);

$total3 = mysql_fetch_assoc($total2);
$app_totals = $total3['app_totals'];
?>
                <tr>
                    <td><?php echo $f1; ?></td>
					<td><?php echo $f2; ?></td>
					<td><?php echo $f3; ?></td>
					<td><?php echo $f4; ?></td>
                    <td><?php echo $f5; ?></td>
                    <td><?php echo $f6; ?></td>
					<td><?php echo $f7; ?></td>
					<td><?php echo $f8; ?></td>
                    <td><?php echo $app_totals; ?></td>
                    <td><?php echo $f10; ?></td>
                    <td><?php
					if ($app_totals <= 100 && $app_totals >= 89) {echo 'Excellent';}
					if ($app_totals <= 88.9 && $app_totals >= 76) {echo 'Very Good';}
					if ($app_totals <= 75.9 && $app_totals >= 63) {echo 'Satisfactory';}
					if ($app_totals <= 62.9 && $app_totals >= 50) {echo 'Average';}
					if ($app_totals <= 49.9 && $app_totals >= 37) {echo 'Needs Improvement';}
					if ($app_totals <= 36.9 && $app_totals >= 24) {echo 'Very Poor';}
					if ($app_totals <= 23.9 && $app_totals >= 11) {echo 'Very Poor';}
					?></td>
					<td> <a onClick="window.open('print.php?p_id=<?php echo $f1; ?>',
                      'newwindow', 'width=800, height=500'); return false;" style=" color: blue; cursor: pointer">View</a></td>
                    
                    
                </tr>
                <?php
				$i++;
				}
				?>
            </tbody>
            
        </table>