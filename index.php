
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<title>Jirah&trade;</title>

<link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
<script type="text/javascript" src="js/login.js"></script>

<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript" src="js/pngFix.js"></script>
<script type="text/javascript">
  DD_belatedPNG.fix('.png-fix');
</script>



</head>
<body>
<script>
$(document).ready(function(){
    $.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase());
    if($.browser.chrome){
        $.browser.safari = false;
    }
    else {
        alert("Please use Google Chrome for Jirah System.");
		window.close();
    }
});
</script>
<img src="images/gradient.jpg" class="bg">
<div id="wrapper">
 			<div id="registration">
                
                  <form method="post" class="signin" action="auth.php?action=login">
                <h2 style="background-color: white"> <img src="images/logo.png"></h2>
                  
                    <fieldset>
                   <h3>Jirah System</h3>
                   
                         <p>
                            <label for="name">Username</label>
                            <input id="name" name="username" type="text" class="text"/>
                         </p>        
                         <p>
                            <label for="password">Password</label>
                            <input id="password" name="password" class="text" type="password" />
                         </p>
                      
                         <p>
                          <label for="password"> Login</label>
                            <input id="submit" class="text" type="submit"  value="Login" >
                         </p>
                    </fieldset>
                
                 </form>
      			 </div>
            
    			 <div id="container">  
                     <div id="overlay" class="png-fix">
                             <div id="title">
                                 City<br />Garden<br />Hotels
                             </div>
                        </div>
 				 </div>
</div>
</body>
</html>